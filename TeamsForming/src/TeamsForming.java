import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This is java solution to the problem: Teams Forming which can be found:
 * https://codeforces.com/problemset/problem/1092/B
 * 
 * @author skywalker
 *
 */
public class TeamsForming
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int studentNumber = scanner.nextInt();

		List<Integer> students = new ArrayList<>(studentNumber);
		for (int i = 0; i < studentNumber; i++)
		{
			students.add(scanner.nextInt());
		}
		scanner.close();

		int max = students.stream().max(Integer::compare).get();

		int[] arr = new int[max + 1];

		students.forEach(s ->
		{
			arr[s] = arr[s] + 1;
		});

		students = new ArrayList<>(studentNumber);

		for (int p = 0; p < arr.length; p++)
		{
			int pos = arr[p];
			if (0 != pos)
			{
				for (int i = 0; i < pos; i++)
				{
					students.add(p);
				}

			}
		}

		Integer totalDiff = 0;
		for (int i = 0; i < students.size() - 1; i = i + 2)
		{
			Integer s1 = students.get(i);
			Integer s2 = students.get(i + 1);

			Integer diff1 = s2 - s1;

			totalDiff = totalDiff + diff1;
		}

		System.out.println(totalDiff);
	}
}
